#include <gcrypt.h>
#include "hash.h"

static gcry_md_hd_t hash_packet(dpacket *dp) {
    gcry_md_hd_t hash;
    // Initialize the hash
    if (GPG_ERR_NO_ERROR != gcry_md_open(&hash, GCRY_MD_SHA256, 0))
        PE_QUIT("Couldn't get SHA-256");

    gcry_md_write(hash, dp->id.id, sizeof(id_number));
    gcry_md_write(hash, dp->data, sizeof(dp->data));
    gcry_md_final(hash);

    return hash;
}

void hash_init(void) {
    gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
}

void fill_out_checksum(dpacket *dp) {
    gcry_md_hd_t hash = hash_packet(dp);

    memcpy(dp->checksum, gcry_md_read(hash, GCRY_MD_SHA256), DINET_CHECKSUM_SZ);
    gcry_md_close(hash);
}

bool valid_checksum(dpacket *dp) {
    gcry_md_hd_t hash = hash_packet(dp);

    bool valid = !memcmp(dp->checksum, gcry_md_read(hash, GCRY_MD_SHA256), DINET_CHECKSUM_SZ);
    gcry_md_close(hash);

    return valid;
}
