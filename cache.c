#include "common.h"
#include "cache.h"

void init_cache(struct lru_cache *cache, int max_items) {
    cache->max_items = max_items;
    cache->cache = NULL;
}

bool in_cache(struct lru_cache *cache, u8 *num, int sz) {
    struct hash_struct *tmp;

    HASH_FIND(hh, cache->cache, num, sz, tmp); 

    if (tmp != NULL) {
        HASH_DELETE(hh, cache->cache, tmp);
        HASH_ADD(hh, cache->cache, key, sz, tmp);
        return true;
    } else {
        return false;
    }
}

int add_to_cache(struct lru_cache *cache, void *data, void *cache_key, int key_len, 
        void **deleted_ptr, bool free_deleted) {
    struct hash_struct *tmp, *tmp_entry;
    int deleted = 0;

    HASH_FIND(hh, cache->cache, cache_key, key_len, tmp);

    if (tmp == NULL) {
        if (HASH_COUNT(cache->cache) >= cache->max_items) {
            HASH_ITER(hh, cache->cache, tmp, tmp_entry) {
                HASH_DELETE(hh, cache->cache, tmp);
                if (deleted_ptr != NULL) {
                    *deleted_ptr = tmp->data;
                    deleted = 1;
                }

                if (free_deleted)
                    free(tmp->data);

                free(tmp);
                break;
            }
        }

        struct hash_struct *hs = dmalloc(sizeof(struct hash_struct));
        memcpy(hs->key, cache_key, key_len);
        hs->data = data;
        HASH_ADD(hh, cache->cache, key, key_len, hs);
    }
    return deleted;
}

