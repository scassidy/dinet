#ifndef __DINET_HASH_H
#define __DINET_HASH_H

#include <stdbool.h>
#include "server.h"

void hash_init(void);
void fill_out_checksum(dpacket *dp);
bool valid_checksum(dpacket *dp);

#endif

