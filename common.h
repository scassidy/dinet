#ifndef __DINET_COMMON_H
#define __DINET_COMMON_H

#include <stdint.h>
#include <stdbool.h>
#include "server.h"

void *get_socket(void *context, int type);
void waitsleep(int milliseconds);
void gen_random(uint8_t *data, size_t len);
void print_packet(char *str, dpacket *dp, bool print_data);
bool receive_more(void *socket);

void *dmalloc(size_t sz);
void *dcalloc(size_t nmemb, size_t sz);
void *drealloc(void *ptr, size_t sz);
#define dfree free

#endif
