#ifndef _DINET_HTTPD_H
#define _DINET_HTTPD_H

#include <stdbool.h>
#include <microhttpd.h>
#include <pthread.h>

#define HTTPD_PUBLISH_INP "inproc://subscribe-httpd"
#define HTTPD_SUBSCRIBE_INP "inproc://publish-httpd"

struct httpd_data {
    void *context;
    bool run;
    pthread_barrier_t barrier;
};

int httpd_handle(void *cls,
        struct MHD_Connection *connection,
        const char *url,
        const char *method,
        const char *version,
        const char *upload_data,
        size_t *upload_data_size,
        void **ptr);

void *httpd_worker(struct httpd_data* const data);
void *httpd_init(int max, int port);
void httpd_destroy(void *handle);

#endif
