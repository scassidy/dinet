#ifndef __DINET_EDGE_H
#define __DINET_EDGE_H

#include <stdbool.h>
#include <pthread.h>

#define EDGE_PUBLISH_INP "inproc://subscribe"
#define EDGE_SUBSCRIBE_INP "inproc://publish"

struct edge_data {
    void *context;
    char **listen_addrs;
    bool run;
    pthread_barrier_t barrier;
};

void *edge_worker(struct edge_data* const data);

#endif
