#include <stdio.h>
#include <string.h>
#include <zmq.h>
#include <assert.h>
#include "server.h"
#include "common.h"
#include "edge.h"
#include "hash.h"
#include "uthash.h"
#include "utarray.h"

#define USER_ID_LEN 8

struct id2prefix {
    u8 user_id[USER_ID_LEN];
    int user_id_len;
    id_number prefix;
    int prefix_len;
    UT_hash_handle hh;
} *id2prefix = NULL;

struct prefix {
    id_number prefix;
    UT_array *users;
    UT_hash_handle hh;
} *prefix2ids = NULL;

UT_icd prefix2ids_helper = { USER_ID_LEN, NULL, NULL, NULL };

static inline struct id2prefix* get_user_id(u8 *key) {
    struct id2prefix *tmp;

    HASH_FIND(hh, id2prefix, key, USER_ID_LEN, tmp);

    return tmp;
}

static inline bool has_user_id(u8 *key) {
    return get_user_id(key) != NULL;
}

static void add_user_id(u8 *key, int key_len, id_number *new_id, int prefix_len) {
    if (!has_user_id(key)) {
        // TODO: Expire old users
        struct id2prefix *is = dcalloc(1, sizeof(struct id2prefix));
        memcpy(is->user_id, key, USER_ID_LEN);
        memcpy(is->prefix.id, new_id->id, DINET_ID_SZ);
        is->user_id_len = key_len;
        is->prefix_len = prefix_len;
        HASH_ADD(hh, id2prefix, user_id, USER_ID_LEN, is);

        // Mask the id
        id_number id;
        bzero(&id.id, sizeof(id));
        memcpy(&id.id, new_id->id, sizeof(id));

        // Add it to the prefix hash
        struct prefix *tmp;
        HASH_FIND(hh, prefix2ids, id.id, DINET_ID_SZ, tmp);
        
        if (tmp == NULL) {
            tmp = dmalloc(sizeof(struct prefix));
            memcpy(tmp->prefix.id, id.id, sizeof(id));
            utarray_new(tmp->users, &prefix2ids_helper);
            HASH_ADD(hh, prefix2ids, prefix, DINET_ID_SZ, tmp);
        }

        int i = 0;
        dprintf("[edge] Adding user ");
        for (i = 0; i < USER_ID_LEN; i++) {
            dprintf("%02x", key[i]);
        }
        dprintf(" prefix: ");
        for (i = 0; i < DINET_ID_SZ; i++) {
            dprintf("%02x", new_id->id[i]);
        }
        dprintf("/%d ", prefix_len);

        utarray_push_back(tmp->users, is->user_id);
        dprintf(" %d users at this prefix now\n", utarray_len(tmp->users)); 
    }
}

static UT_array* get_users_from_packet(dpacket *dp) {
    int i = 0;
    UT_array *ret;
    struct prefix *tmp;
    id_number id;

    utarray_new(ret, &prefix2ids_helper);

    memcpy(&id.id, dp->id.id, DINET_ID_SZ);
    for(i = 0; i < DINET_ID_SZ - 1; i++) {
        HASH_FIND(hh, prefix2ids, id.id, DINET_ID_SZ, tmp);
        if (tmp != NULL) {
            // This is terrible
            u8 *uid;
            for (uid = (u8*)utarray_front(tmp->users); uid != NULL;
                 uid = (u8*)utarray_next(tmp->users,uid)) {
                u8 *ruid;
                bool match = false;
                for (ruid = (u8*)utarray_front(ret); ruid != NULL;
                     ruid = (u8*)utarray_next(ret, ruid)) {
                    if (!memcmp(uid, ruid, USER_ID_LEN)) {
                        match = true;
                        break;
                    }
                }
                if (!match)
                    utarray_push_back(ret, uid);
            }
        }

        id.id[DINET_ID_SZ - i - 1] = 0;
    }

    return ret;
}

void print_multimsg(zmq_msg_t *message) {
    // Dump the message as text or binary
    char *data = zmq_msg_data (message);
    int size = zmq_msg_size (message);
    int is_text = 1;
    int char_nbr;
    for (char_nbr = 0; char_nbr < size; char_nbr++)
        if ((unsigned char) data [char_nbr] < 32
                || (unsigned char) data [char_nbr] > 127)
            is_text = 0;

    d2printf ("[edge] [%03d] ", size);
    for (char_nbr = 0; char_nbr < size; char_nbr++) {
        if (is_text) {
            d2printf ("%c", data [char_nbr]);
        }
        else {
            d2printf ("%02X", (unsigned char) data [char_nbr]);
        }
    }
    d2printf ("\n");
}

int send_to_edge_user(void *socket, u8 id[], int len, dpacket *dp) {
    zmq_msg_t message;
    zmq_msg_init_size(&message, len);
    memcpy(zmq_msg_data(&message), id, len);
    int sz = zmq_sendmsg(socket, &message, ZMQ_SNDMORE);
    assert(sz == len);
    zmq_msg_close(&message);

    zmq_msg_init_size(&message, sizeof(dpacket));
    memcpy(zmq_msg_data(&message), dp, sizeof(dpacket));
    assert(sizeof(dpacket) == zmq_sendmsg(socket, &message, 0));
    zmq_msg_close(&message);

    dprintf("[edge] sent to edge user, %d: ", len);
    int i = 0;
    for (; i < len; i++) {
        dprintf("%02x", id[i]);
    }
    dprintf("\n");
    return 0;
}


void *edge_worker(struct edge_data * const args) {
    int i;
    char **router_listen = args->listen_addrs;
    void *context = args->context;

    // Sends messages via INPROC to the dinetd router
    void *publisher = get_socket(context, ZMQ_PUB);

    // Receives messages via INPROC from the dinetd router
    void *subscriber = get_socket(context, ZMQ_SUB);

    // Receives messages from clients
    void *router = get_socket(context, ZMQ_ROUTER);

    zmq_bind(publisher, EDGE_PUBLISH_INP);
    zmq_connect(subscriber, EDGE_SUBSCRIBE_INP);
    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, 0, 0);

    for(i = 0; router_listen[i] != NULL; i++) {
        zmq_bind(router, router_listen[i]);
    }

    pthread_barrier_wait(&args->barrier);

    zmq_pollitem_t items[] = {
        { subscriber, 0, ZMQ_POLLIN, 0 },
        { router, 0, ZMQ_POLLIN, 0 }
    };

    while(args->run) {
        u8 user_id[USER_ID_LEN];
        int user_id_len;
        id_number id;
        int prefix_len;
        dpacket dp;

        zmq_msg_t message;
        zmq_msg_init(&message);

        zmq_poll (items, 2, 1000);

        if (items[0].revents & ZMQ_POLLIN) {
            // Got a message from the dinet router
            zmq_recvmsg(subscriber, &message, 0);
            assert(zmq_msg_size(&message) == sizeof(dp));
            memcpy(&dp, zmq_msg_data(&message), zmq_msg_size(&message));

            print_packet("[edge] sending to edge", &dp, false);

            UT_array *users = get_users_from_packet(&dp);

            dprintf("[edge] sending to %d users\n", utarray_len(users));
            u8 *uid;
            for (uid = (u8*)utarray_front(users); uid != NULL;
                 uid = (u8*)utarray_next(users,uid)) {
                struct id2prefix *user =  get_user_id(uid);
                assert(user != NULL);

                send_to_edge_user(router, uid, user->user_id_len, &dp); // todo
            }
        }

        if (items[1].revents & ZMQ_POLLIN) {
            // Got a message from the client
            bzero(user_id, USER_ID_LEN);
            user_id_len = zmq_recvmsg(router, &message, 0);
            assert(zmq_msg_size(&message) <= sizeof(user_id));
            memcpy(user_id, zmq_msg_data(&message), zmq_msg_size(&message));

            assert(receive_more(router));
            zmq_msg_close(&message);
            zmq_msg_init(&message);

            if (has_user_id(user_id)) {
                // They're sending a message
                int sz = zmq_recvmsg(router, &message, 0);
                dprintf("[edge] %d, %d\n", sz, (int)zmq_msg_size(&message));
                if (zmq_msg_size(&message) == sizeof(dpacket)) {
                    memcpy(&dp, zmq_msg_data(&message), zmq_msg_size(&message));

                    if (valid_checksum(&dp)) {
                        print_packet("[edge] forwarding", &dp, false);
                        zmq_msg_init_size(&message, sizeof(dp));
                        memcpy(zmq_msg_data(&message), &dp, sizeof(dp));
                        zmq_sendmsg(publisher, &message, 0);
                        zmq_msg_close(&message);
                    }
                }
                zmq_msg_close(&message);
            } else {
                // This is a hello
                int sz = zmq_recvmsg(router, &message, 0);
                d2printf("[edge] %d, %d: %.*s\n", sz,
                        (int)zmq_msg_size(&message),
                        (int)zmq_msg_size(&message),
                        (char*)zmq_msg_data(&message));
                if (zmq_msg_size(&message) == sizeof(id)) {
                    memcpy(id.id, zmq_msg_data(&message), zmq_msg_size(&message));

                    assert(receive_more(router));
                    zmq_msg_init(&message);

                    zmq_recvmsg(router, &message, 0);
                    assert(zmq_msg_size(&message) == sizeof(prefix_len));
                    memcpy(&prefix_len, zmq_msg_data(&message), zmq_msg_size(&message));

                    add_user_id(user_id, user_id_len, &id, prefix_len);
                }
                zmq_msg_close(&message);
            }
        }
    }

    dprintf("[edge] Exiting\n");

    zmq_close(publisher);
    zmq_close(subscriber);
    zmq_close(router);

    return NULL;
}

#ifdef EDGE_MAIN
int main(int argc, char **argv)
{
    char *router_listen[2];
    router_listen[0] = strdup(argv[1]);
    router_listen[1] = NULL;

    edge_worker(router_listen);

    return 0;
}
#endif
