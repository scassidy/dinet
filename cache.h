#ifndef __DINET_CACHE_H
#define __DINET_CACHE_H

#include "uthash.h"
#include "server.h"
#include <stdbool.h>

struct hash_struct {
    u8 key[DINET_CHECKSUM_SZ];
    void *data;
    UT_hash_handle hh;
};

struct lru_cache {
    int max_items;
    struct hash_struct *cache;
};

void init_cache(struct lru_cache *cache, int max_items);
bool in_cache(struct lru_cache *cache, u8 *num, int sz);
int add_to_cache(struct lru_cache *cache, void *data, void *cache_key, 
        int key_len, void **deleted_ptr, bool free_deleted);

#endif

