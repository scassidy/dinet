CFLAGS=-Wall -ggdb -O0 -pipe -Wextra -Wno-char-subscripts -DURL_PREFIX="\"dinet\""
ODIR=obj
LIBS=-lzmq -lgcrypt -lmicrohttpd -pthread
HDRS=server.h hash.h common.h edge.h httpd.h cache.h base64.h ratelimit.h
OBJ=server.o hash.o common.o edge.o httpd.o cache.o base64.o ratelimit.o
SUBDIR_OBJ=radix/radix.o jsmn/jsmn.o

_OBJ = $(patsubst %,$(ODIR)/%,$(OBJ))

$(ODIR)/%.o: %.c $(HDRS)
	$(CC) -c -o $@ $< $(CFLAGS)

all: directories radix jsmn dinetd

dinetd: $(_OBJ) $(SUBDIR_OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: directories clean 

directories:
	mkdir -p $(ODIR)

client: force
	$(MAKE) -C client

radix: force
	$(MAKE) -C radix

jsmn: force
	$(MAKE) -C jsmn

clean:
	rm -f $(ODIR)/*.o *~ core dinetd
	$(MAKE) -C client clean
	$(MAKE) -C radix clean
	$(MAKE) -C jsmn clean

force:
	true
