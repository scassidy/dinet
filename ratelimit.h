#ifndef __RATE_LIMIT_H
#define __RATE_LIMIT_H

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "common.h"
#include "uthash.h"

struct minute_map {
    u8 address[16];
    int count;
    UT_hash_handle hh;
};

struct ratelimit {
    size_t threshold;
    int number;
    int divisor;
    struct minute_map **maps;
    time_t last_accessed;
};

struct ratelimit *ratelimit_init(size_t threshold, int number, int divisor);

/**
 * ratelimit_check
 *
 * Returns true if this key has been used too much, false if it's under
 * the threshold
 */
bool ratelimit_check(struct ratelimit* handle, void *key, size_t key_len);

void ratelimit_deinit(struct ratelimit *handle);

#endif

