#include <ctype.h>
#include <stdint.h>
#include "base64.h"
#include "common.h"

static u8 encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static size_t mod_table[] = {0, 2, 1};

char *base64_encode(const u8 *data,
        size_t input_length,
        size_t *output_length) {

    *output_length = ((input_length - 1) / 3) * 4 + 4;

    char *encoded_data = dmalloc(*output_length + 1);
    if (encoded_data == NULL) return NULL;

    size_t i, j;
    for (i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? data[i++] : 0;
        uint32_t octet_b = i < input_length ? data[i++] : 0;
        uint32_t octet_c = i < input_length ? data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    encoded_data[*output_length] = '\0';

    return encoded_data;
}

void* base64_init() {
    char *decoding_table = dmalloc(256);

    int i = 0;
    for (; i < 0x40; i++)
        decoding_table[encoding_table[i]] = i;

    return decoding_table;
}

void base64_cleanup(void *handle) {
    free(handle);
}

u8 *base64_decode(const char *data,
        size_t input_length,
        size_t *output_length, void *handle) {
    char *decoding_table = (char*)handle;

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    u8 *decoded_data = dmalloc(*output_length);

    size_t i = 0, j = 0;
    for (; i < input_length;) {
        if (!isalnum(data[i]) && data[i] != '+' && data[i] != '/' && data[i] != '=') {
            // Invalid input
            free(decoded_data);
            return NULL;
        }

        uint32_t sextet_a = data[i] == '=' ? 0 : decoding_table[data[i]]; i++;
        uint32_t sextet_b = data[i] == '=' ? 0 : decoding_table[data[i]]; i++;
        uint32_t sextet_c = data[i] == '=' ? 0 : decoding_table[data[i]]; i++;
        uint32_t sextet_d = data[i] == '=' ? 0 : decoding_table[data[i]]; i++;

        uint32_t triple = (sextet_a << 3 * 6)
            + (sextet_b << 2 * 6)
            + (sextet_c << 1 * 6)
            + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

