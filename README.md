# Diluvian Network

Diluvian Network (DiNet) attempts facilitate the anonymous encrypted communication of two or more parties in a scalable way. Read [more about DiNet](https://bitbucket.org/scassidy/dinet/wiki/DiNet%20Overview), which includes design decisions and background information.  

**This is still beta quality software**. As with most software, especially cryptography and anonymity software, do not trust your life to it until you have verified it yourself.

## Try out DiNet

There is a [sample client available](http://projects.existentialize.com/dinet/client.html) which by default points to the main Diluvian Network. It's pretty rough at this point, but it's easy to use.

1. Pick a shared prefix. This will be used to limit how much bandwidth your client consumes. Each message you send (with this client, clients can choose to do whatever they wish), this prefix will be used on every message you send and you will receive all messages beginning with this prefix.
2. Pick a shared password. This will authenticate and decrypt messages, and since messages can be seen by everyone, you should pick a good one!
3. Type a name and a message and press submit. The client will automatically poll for messages now.

You can also [download the client](https://bitbucket.org/scassidy/dinet-client/overview) to your own machine.

# Using the server

## Compilation

You will need:

* ZeroMQ >= 3.0
* libgcrypt >= 1.5.0
* libmicrohttpd
* pthread support
* libncurses for the text based client (optional)

Run:

    $ make

Easy!

## Starting the server

The server program, dinetd, subscribes to other dinetd nodes and exposes 
interfaces to clients. See the [detailed compliation information](https://bitbucket.org/scassidy/dinet/wiki/Running%20your%20own%20DiNet%20Network) on the wiki.

To have two servers subscribe and publish back to one another, the following
commands would be used:

    $ #server1
    $ dinetd -s server2.com:4567 -l \*:4567 -w 1000
    
    $ #server2
    $ dinetd -s server1.com:4567 -l \*:4567 -w 1000

The servers can be started in any order. The *-w* parameter generates fake traffic to hide real traffic. To have another server publish an HTTP endpoint, the following command would be used: 

    $ #server3
    $ dinetd -s server1.com:4567 -H 8080 -w 1000

Then, clients would set their HTTP API applications at http://server3.com:8080.

## Using DiNet

There is one main DiNet client written for the HTTP API, which is the [dinet JQuery client](https://bitbucket.org/scassidy/dinet-client). To use it, simply type in a password shared by all participants, pick a prefix that is also shared, and hit Submit to send messages.

