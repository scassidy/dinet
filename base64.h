#ifndef __DINET_BASE64_H
#define __DINET_BASE64_H

#include <stdlib.h>
#include "common.h"

char *base64_encode(const u8 *data,
        size_t input_length,
        size_t *output_length);

void* base64_init();
void base64_cleanup(void *handle);
u8 *base64_decode(const char *data,
        size_t input_length,
        size_t *output_length, void *handle);
#endif
