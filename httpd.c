#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <zmq.h>
#include <assert.h>
#include <sys/param.h>
#include <ctype.h>
#include <netinet/in.h>
#include "common.h"
#include "httpd.h"
#include "radix/radix.h"
#include "cache.h"
#include "base64.h"
#include "hash.h"
#include "jsmn/jsmn.h"
#include "ratelimit.h"

static pthread_mutex_t trie_lock;
static struct radix_node root;
static struct lru_cache cache;
static void *b64_handle;
static void *publisher;
static struct MHD_Response *blank_response;
static struct ratelimit *limiter;

struct entry {
    time_t time;
    dpacket dp;
};

#define PAGE "<html><head><title>libmicrohttpd demo</title>"\
    "</head><body>libmicrohttpd demo</body></html>"

void *httpd_init(int max, int port) {
    if (pthread_mutex_init(&trie_lock, NULL)) {
        fprintf(stderr, "Couldn't create mutex: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    radix_init(&root); 
    init_cache(&cache, max);
    b64_handle = base64_init();
    blank_response = MHD_create_response_from_data(0, "", MHD_NO, MHD_NO);
    limiter = ratelimit_init(100, 3, 30);
    struct MHD_Daemon *httpd = MHD_start_daemon(
            MHD_USE_SELECT_INTERNALLY |
            (debug >= 2 ? MHD_USE_DEBUG : 0),
            port, NULL, NULL,
            &httpd_handle,
            PAGE,
            MHD_OPTION_END);
    return httpd;
}

void httpd_destroy(void *handle) {
    MHD_stop_daemon((struct MHD_Daemon*)handle);
}

static void string2bytes(const char *str, int len, u8 *out) {
    int i = 0;
    for(; i < len; i += 2, str += 2)
        sscanf(str, "%02x", (unsigned int*)out++);
}

static void json_struct_entry(char **str, size_t *max_size, int *offset, struct entry *entry) {
    const char *hdr = "{\"time\":\"";
    const char *mid = "\",\"data\":\"";
    const char *mid2 = "\",\"id\":\"";
    const char *mid3 = "\",\"checksum\":\"";
    const char *ftr = "\"}";
    size_t b64_len;
    char *base64 = base64_encode(entry->dp.data, DINET_DATA_SZ, &b64_len);

    size_t req_len = b64_len + strlen(hdr) + strlen(mid) + strlen(mid2) + strlen(mid3) +
        strlen(ftr) + 21 + DINET_ID_SZ * 2 + DINET_CHECKSUM_SZ * 2;

    if (*max_size - *offset - 1 < req_len) {
        int new_size = MAX(*max_size * 2, *max_size + req_len);
        d3printf("[httpd] Current size: %lu, req: %llu, new: %d\n", 
                *max_size - *offset - 1, (long long unsigned)req_len, new_size);
        *str = drealloc(*str, new_size);
        *max_size = new_size;
    }

    d3printf("[httpd] Writing message for time %lld: %s\n", (long long)entry->time, base64); 
    *offset += snprintf(*str + *offset, *max_size - *offset,
            "%s%lld%s%s%s", hdr, (long long)entry->time,
            mid, base64, 
            mid2);

    int i = 0;
    for (; i < DINET_ID_SZ; i++) {
        *offset += snprintf(*str + *offset, *max_size - *offset,
                "%02x", entry->dp.id.id[i]);
    }

    *offset += snprintf(*str + *offset, *max_size - *offset, "%s", mid3);

    for (i = 0; i < DINET_CHECKSUM_SZ; i++) {
        *offset += snprintf(*str + *offset, *max_size - *offset,
                "%02x", entry->dp.checksum[i]);
    }

    *offset += snprintf(*str + *offset, *max_size - *offset, "%s", ftr);

    free(base64);
}

static char *json_format_post(dpacket *dp) {
    const char *hdr = "{\"checksum\":\"";
    const char *ftr = "\"}";
    const size_t sz = strlen(hdr) + strlen(ftr) + DINET_CHECKSUM_SZ * 2 + 2;
    char *ret = dmalloc(sz);
    int offset = strlen(hdr);

    strcpy(ret, hdr);

    int i = 0;
    for (; i < DINET_CHECKSUM_SZ; i++) {
        snprintf(ret + offset, sz - offset, "%02x", dp->checksum[i]);
        offset += 2;
    }

    strcat(ret + offset, ftr);
    return ret;
}

static char *json_format_response(struct entry **entry, int items, time_t after_this) {
    size_t max_size = (items + 1) * 50 + 1;
    char *result = dcalloc(max_size--, 1);
    int offset = 0;  
    int i = 0;

    strcpy(result, "{\"results\":[");
    offset = strlen(result);

    d3printf("Formatting JSON with %d items after %llu time\n", items, (unsigned long long)after_this);
    for(; i < items; i++) {
        if (entry[i]->time >= after_this) { 

            d3printf("input offset: %d\n", offset);
            json_struct_entry(&result, &max_size, &offset, entry[i]);
            d3printf("output offset: %d\n", offset);


            if (max_size - offset < 2) {
                d3printf("Resize on margin to %llu\n", (unsigned long long)max_size);
                max_size *= 2;
                result = drealloc(result, max_size);
            }

            if (i < items - 1)
                result[offset++] = ',';

            d3printf("Current, #%d, %*s\n", i, offset, result);
        } else {
            d3printf("Time wrong, %llu < %llu\n",
                    (unsigned long long)entry[i]->time, (unsigned long long)after_this);
        }
    }

    result[offset++] = ']';
    result[offset++] = '}'; 
    result[offset] = '\0';

    return result;
}


enum json_result {
    json_failure = -1,
    json_success,
    json_continue
};

static enum json_result json_parse(const char *input, u8 *key, const char **data) {
    jsmntok_t tokens[10];
    jsmn_parser json;
    jsmn_init(&json);
    bool got_id = false, got_data = false;

    int r = jsmn_parse(&json, input, tokens, 10);

    if (r != JSMN_SUCCESS) {
        dprintf("JSMN failed: \"%s\"\n", input);
        return json_continue;
    }

    // Look for id or data
    int i = 0;
    while (i < 10 && (!got_id || !got_data)) {
        if (tokens[i].type == JSMN_STRING) {
            // Check if it's id or data
            if (!got_id && !strncmp(&input[tokens[i].start], "id", 2)) {
                // Got ID
                i++;
                if (tokens[i].type == JSMN_STRING) {
                    got_id = true;
                    string2bytes(input + tokens[i].start, DINET_ID_SZ*2, key);
                } else {
                    d2printf("[httpd] Got ID, but token %d wasn't a string but a %d\n", i, tokens[i].type);
                    return json_failure;
                }
            } else if (!got_data && !strncmp(&input[tokens[i].start], "data", 4)) {
                // Got data
                i++;
                if (tokens[i].type == JSMN_STRING) {
                    *data = input + tokens[i].start;
                    got_data = true;
                } else {
                    d2printf("[httpd] Got data, but token %d wasn't a string but a %d\n", i, tokens[i].type);
                    return json_failure;
                }
            }
        }
        i++;
    }

    return json_success;
}


// URL format: http://host/whatever/doesnt/matter/prefix/seconds
// Example: http://dinet.org/hello/37db29/30
static bool parse_url(const char *url, const char *match, u8 *id_prefix, int *prefix_len, int *before_seconds) {
    char *start;    
    char *before;                                             
    char *prefix;   

    if ((start = strstr(url, match)) != NULL) {
        prefix = start + strlen(match) + 1; // move past the match
        if ((before = strchr(prefix, '/')) != NULL) {        
            if (prefix_len == NULL && id_prefix == NULL && before_seconds == NULL)
                return true;

            int len = *prefix_len;                
            *prefix_len = (before - prefix) & ~1; // make even

            before++;
            *before_seconds = (int)strtol(before, NULL, 0);  

            if (len >= *prefix_len) {             
                string2bytes(prefix, *prefix_len, id_prefix);
                return true; 
            }       
        }           
    }               

    return false;   
}            

enum conn_type {
    ERROR = 0,
    GET,
    POST,
    OPTIONS
};

struct conn_info {
    enum conn_type type;
    char *answer;
    size_t offset;
    size_t sz;
};

int handle_post(struct conn_info *info, const char *data, size_t size) {
    // Parse upload_data
    dpacket dp;
    const char *quote;

    if (info->sz - info->offset < size) {
        info->sz = info->sz*2 + size + 1;
        info->answer = drealloc(info->answer, info->sz);
        d3printf("[httpd] Allocing %lu bytes for POST processing\n", (long unsigned)info->sz);
    }

    strncpy(info->answer + info->offset, data, size);
    info->offset += size;
    info->answer[info->offset] = '\0';

    const char *result_str;
    enum json_result result = json_parse(info->answer, dp.id.id, &result_str);

    if (result == json_success && result_str != NULL && (quote = strchr(result_str, '"')) != NULL) {
        int len = quote - result_str;
        size_t olen;
        u8 *raw_data = base64_decode(result_str, len, &olen, b64_handle);

        free(info->answer);
        info->answer = NULL;
        info->offset = 0;
        info->sz = 0;

        if (raw_data == NULL) {
            dprintf("[httpd] couldn't decode base64: \"%s\" len: %d\n", result_str, len);
            return MHD_NO;
        }

        // Send packet
        memcpy(dp.data, raw_data, olen);
        free(raw_data);
        bzero(dp.data + olen, DINET_DATA_SZ - olen);
        fill_out_checksum(&dp);

        zmq_msg_t message;
        zmq_msg_init_size(&message, sizeof(dp));
        memcpy(zmq_msg_data(&message), &dp, sizeof(dp));
        zmq_sendmsg(publisher, &message, 0);
        zmq_msg_close(&message);

        // Format result
        info->answer = json_format_post(&dp);
        d3printf("[httpd] Formatted JSON result: \"%s\"\n", info->answer);
        return MHD_NO;
    } else {
        return MHD_YES;
    }
}

static inline bool rate_limit(const union MHD_ConnectionInfo *info) {
    struct sockaddr *in = info->client_addr;
    if (in->sa_family == AF_INET) {
        // IPv4
        struct sockaddr_in *ipv4 = (struct sockaddr_in*)in;
        return ratelimit_check(limiter, &ipv4->sin_addr, sizeof(struct in_addr));
    } else if (in->sa_family == AF_INET6) {
        // IPv6
        struct sockaddr_in6 *ipv6 = (struct sockaddr_in6*)in;
        return ratelimit_check(limiter, &ipv6->sin6_addr, sizeof(struct in6_addr));
    } else {
        dprintf("Received unknown sa_family: %d\n", in->sa_family);
        return true; // deny unknown requests
    }
}

#define TYPE_HTML "text/html"
#define TYPE_JSON "application/json"
#define ADD_HEADERS(response, type) \
    do { \
        MHD_add_response_header (response, "Content-Type", type "; charset=utf-8"); \
        MHD_add_response_header (response, "Access-Control-Allow-Origin", "*"); \
    } while (0)

const char *response_400 = "<html><body>Bad request</body></html>";
const char *response_429 = "<html><body>Too many requests</body></html>";
const char *response_500 = "<html><body>Internal server error</body></html>";

#ifndef URL_PREFIX
#define URL_PREFIX "whatever"
#endif

int httpd_handle(void *cls,
        struct MHD_Connection *connection,
        const char *url,
        const char *method,
        const char *version,
        const char *upload_data,
        size_t *upload_data_size,
        void **ptr) {
    struct conn_info *info;
    struct MHD_Response *response = NULL;
    int ret;
    time_t now = time(NULL);
    const union MHD_ConnectionInfo *client_info = MHD_get_connection_info(connection, 
            MHD_CONNECTION_INFO_CLIENT_ADDRESS);


    d3printf("[httpd] HTTP: %s %s %s\n", method, url, version);

    if (!strcmp(method, MHD_HTTP_METHOD_OPTIONS)) {
        // OPTIONS request
        response = MHD_create_response_from_data(0,
                (void*) "", MHD_NO, MHD_NO);

        d3printf("[httpd] Options, url: %s\n", url);
        if (!strcmp(url, "*")) {
            MHD_add_response_header(response, "Allows", "OPTIONS, GET, HEAD, POST");
        } else if (!strcmp("/" URL_PREFIX, url) || !strcmp("/" URL_PREFIX "/", url)) {
            MHD_add_response_header(response, "Allows", "OPTIONS, POST");
        } else if (parse_url(url, URL_PREFIX, NULL, NULL, NULL)) {
            MHD_add_response_header(response, "Allows", "OPTIONS, GET, HEAD");
        } else {
            goto return_400;
        }

        ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
        goto end;
    }

    // These requests need two goes
    if (*ptr == NULL) {
        *ptr = dcalloc(1, sizeof(struct conn_info));
        return MHD_YES;
    }

    info = *ptr;

    if (!strcmp(method, MHD_HTTP_METHOD_POST)) {
        info->type = POST;

        // TODO: make threadsafe if we're not only using SELECT
        if (rate_limit(client_info)) {
            d2printf("[httpd] Denied!\n");
            goto return_429;
        } 

        // process POST request
        if (*upload_data_size > 0) {
            handle_post(info, upload_data, *upload_data_size);
            *upload_data_size = 0;
            return MHD_YES;
        } else if (info->answer != NULL) {
            d3printf("[httpd] POST response: \"%s\"\n", info->answer);
            response = MHD_create_response_from_data(strlen(info->answer),
                    (void*) info->answer,
                    MHD_YES, // free after using
                    MHD_NO);
            ADD_HEADERS(response, TYPE_JSON);
            ret = MHD_queue_response(connection,
                    MHD_HTTP_OK,
                    response);
            goto end;
        } else {
            dprintf("[httpd] Answer is null, returning 400\n");
            goto return_400;
        }
    }

    *ptr = NULL; /* clear context pointer */

    // GET or HEAD
    if (0 != strcmp(method, MHD_HTTP_METHOD_GET) && 0 != strcmp(method, MHD_HTTP_METHOD_HEAD))
        goto return_400;
    if (0 != *upload_data_size)
        goto return_400;

    struct entry *ptrs[1024];
    u8 prefix[DINET_ID_SZ];
    int prefix_len = DINET_ID_SZ;
    int before_sec = 0;
    if (!pthread_mutex_lock(&trie_lock)) {
        info->type = GET;
        // GET or HEAD
        if (parse_url(url, URL_PREFIX, prefix, &prefix_len, &before_sec)) {
            int i = 0;
            d3printf("[httpd] get ");
            for (; i < prefix_len / 2; i++)
                d3printf("%02x:", prefix[i]);
            int r = radix_lookup(&root, prefix, prefix_len / 2, (void**)ptrs, 1024);
            d3printf("[httpd] Got prefix len %d, before_sec %d, r: %d\n", prefix_len, before_sec, r);
            // Format result
            char *json = json_format_response(ptrs, r, now - before_sec);
            response = MHD_create_response_from_data(strlen(json),
                    (void*) json,
                    MHD_YES, // free after using
                    MHD_NO);
            ADD_HEADERS(response, TYPE_JSON);
            ret = MHD_queue_response(connection,
                    MHD_HTTP_OK,
                    response);
            pthread_mutex_unlock(&trie_lock);
            goto end;
        } else {
            d2printf("[httpd] Couldn't parse %s\n", url);
            pthread_mutex_unlock(&trie_lock);
            goto return_400;
        }
    } else {
        fprintf(stderr, "Couldn't lock! %s\n", strerror(errno));
        goto return_500;
    }

return_500:
    // return 500
    response = MHD_create_response_from_data(strlen(response_500),
            (char*)response_500, MHD_NO, MHD_NO);
    ADD_HEADERS(response, TYPE_HTML);
    ret = MHD_queue_response(connection,
            MHD_HTTP_INTERNAL_SERVER_ERROR, response);
    goto end;

return_429:
    // return 429
    response = MHD_create_response_from_data(strlen(response_429),
            (char*)response_429, MHD_NO, MHD_NO);
    ADD_HEADERS(response, TYPE_HTML);
    ret = MHD_queue_response(connection, 429, response);
    goto end;

return_400:
    // return 400
    response = MHD_create_response_from_data(strlen(response_400),
            (char*)response_400, MHD_NO, MHD_NO);
    ADD_HEADERS(response, TYPE_HTML);
    ret = MHD_queue_response(connection,
            MHD_HTTP_BAD_REQUEST, response);
    goto end;

end:
    if (response != NULL) {
        MHD_destroy_response(response);
    }
    return ret;
}

void *httpd_worker(struct httpd_data* const data) {
    void *context = data->context;

    // Sends messages via INPROC to the dinetd router
    publisher = get_socket(context, ZMQ_PUB);

    // Receives messages via INPROC from the dinetd router
    void *subscriber = get_socket(context, ZMQ_SUB);

    zmq_bind(publisher, HTTPD_PUBLISH_INP);
    zmq_connect(subscriber, HTTPD_SUBSCRIBE_INP);
    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, 0, 0);

    pthread_barrier_wait(&data->barrier);

    struct entry *oldentry = NULL;
    while (data->run) {
        struct entry *entry = dmalloc(sizeof(struct entry));
        zmq_msg_t message;
        zmq_msg_init(&message);
        zmq_recvmsg(subscriber, &message, 0);

        if (zmq_msg_size(&message) != sizeof(dpacket)) {
            dprintf("[httpd] received packet of incorrect size %ld from router\n",
                    (long)zmq_msg_size(&message));
            zmq_msg_close(&message);
            continue;
        }

        memcpy(&entry->dp, zmq_msg_data(&message), zmq_msg_size(&message));
        zmq_msg_close(&message);

        entry->time = time(NULL);
        assert(-1 != entry->time);

        if (!pthread_mutex_lock(&trie_lock)) {
            // Add message to trie
            if (add_to_cache(&cache, entry, entry->dp.id.id, DINET_ID_SZ, (void**)&oldentry, false))  {
                d2printf("[httpd] Deleted to make room\n");
                if (RADIX_OK != radix_delete(&root, oldentry->dp.id.id, DINET_ID_SZ, oldentry)) {
                    dprintf("[httpd] Couldn't delete from trie\n");
                }
                free(oldentry);
                oldentry = NULL;
            } else {
                d3printf("[httpd]: time: %lld\n", (long long int)entry->time);
            }

            if (RADIX_OK != radix_insert(&root, entry->dp.id.id, DINET_ID_SZ, entry, false)) {
                print_packet("[httpd]: couldn't insert", &entry->dp, false);
            }
            pthread_mutex_unlock(&trie_lock);
        }  else {
            fprintf(stderr, "Couldn't lock! %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    dprintf("[httpd] exit\n");
    zmq_close(publisher);
    zmq_close(subscriber);

    radix_delete(&root, NULL, 0, NULL); // free the radix trie
    ratelimit_deinit(limiter);
    free(limiter);
    base64_cleanup(b64_handle);
    MHD_destroy_response(blank_response);
    return NULL;
}
