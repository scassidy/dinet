#ifndef __DINET_RADIX_H
#define __DINET_RADIX_H

#define RADIX_NONE -1
#define RADIX_OK 0

#include <stdint.h>

struct radix_node {
    uint8_t *data;
    int len;
    struct radix_node **nodes;
    unsigned int num_nodes:8;
    bool free_ptr;
    void **ptr;
    int num_ptrs;
    size_t ptr_sz;
};

void radix_init(struct radix_node *root);
int radix_lookup(struct radix_node *root, uint8_t *prefix, int prefix_len, 
        void **result, int max_results);
int radix_insert(struct radix_node *root, uint8_t *key, int key_len, void *data, bool free_ptr);
int radix_delete(struct radix_node *root, uint8_t *key, int key_len, void *data);

#endif

