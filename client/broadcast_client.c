#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <zmq.h>
#include <ncurses.h>
#include <assert.h>
#include <time.h>
#include <gcrypt.h>
#include "common.h"
#include "server.h"
#include "hash.h"

int debug;
const char salt[] = {0x9c, 0x22, 0xef, 0xaa, 0xe2, 0xa9, 0x7a, 0x0b, 0xd5, 
    0x8b, 0xe3, 0x6c, 0xdf, 0x63, 0x84, 0x91, 0x98, 0xe7, 0x26, 0x1c, 0x3d,
    0xd7, 0x22, 0xf6, 0x10, 0xda, 0x84, 0x58, 0x59, 0xa1, 0x35, 0xca };

WINDOW *create_newwin(int height, int width, int starty, int startx, bool use_box);

void print_help(void) {
    printf("dinet broadcast client v0.1\n");
    printf("Usage: broadcast_client -c example.com:4567 -n nickname -f abb1230\n");
    printf("\n-c host:port\t\tConnect to dinet edge node at host:port\n");
    printf("-n nickname\t\tSets your nickname to show to other clients\n");
    printf("-f filter\tSets the ID number prefix to use\n");
    printf("-h\t\t\tthis help\n");
}

int connect_to_dinet(void *socket, u8 *filter, int len) {
    int rc;
    zmq_msg_t message;
    zmq_msg_init_size (&message, 16);
    memcpy (zmq_msg_data (&message), filter, 16);
    rc = zmq_sendmsg(socket, &message, ZMQ_SNDMORE);
    zmq_msg_close (&message);

    zmq_msg_init_size(&message, sizeof(len));
    memcpy(zmq_msg_data(&message), &len, sizeof(len));
    rc = zmq_sendmsg(socket, &message, 0);
    zmq_msg_close(&message);

    return (rc);
}

static u8 byte_to_string(char *str, int offset) {
    char buf[3];

    strncpy(buf, str + offset * 2, 2);
    buf[2] = '\0';

    return (u8)strtol(buf, NULL, 16);
}

#define GEXIT(str, err) fprintf(stderr, str "/%s\n", gcry_strerror(err)), exit(EXIT_FAILURE)

static gcry_md_hd_t compute_hmac(char *key, int keysize, u8 *data, int datasize) {
    gcry_md_hd_t hmac;
    gcry_error_t err;
    // — Function: gcry_error_t gcry_md_open (gcry_md_hd_t *hd, int algo,
    // unsigned int flags)
    if ((err = gcry_md_open(&hmac, GCRY_MD_SHA256, GCRY_MD_FLAG_HMAC))) {
        GEXIT("Crypto error: hmac open", err);
    }

    if ((err = gcry_md_setkey(hmac, key, keysize))) {
        GEXIT("Crypto: error: hmac key", err);
    }

    gcry_md_write(hmac, (char*)data, datasize);
    return hmac;
}

static int send_message(void *context, char *nickname, char *msg, char *key, int keysize, u8 *filter, int filter_len) {
    dpacket dp;
    gcry_error_t err;
    char buf[256];
    snprintf(buf, sizeof(buf) - 1, "%s> %s", nickname, msg);

    // Gen random IV
    int blen = filter_len / 2;
    memcpy(dp.id.id, filter, blen);
    gen_random(dp.id.id + blen, sizeof(dp.id.id) - blen);

    // Encrypt
    gcry_cipher_hd_t aes;
    if ((err = gcry_cipher_open(&aes, GCRY_CIPHER_AES256, GCRY_CIPHER_MODE_CTR, 0))) {
        GEXIT("Crypto error: open", err);
    }

    if ((err = gcry_cipher_setkey(aes, key, keysize))) {
        GEXIT("Crypto error: set key", err);
    }

    if ((err = gcry_cipher_setctr(aes, dp.id.id, sizeof(dp.id.id)))) {
        GEXIT("Crypto error: set ctr", err);
    }

    if ((err = gcry_cipher_encrypt(aes, dp.data + 32, sizeof(dp.data) - 32, buf, sizeof(buf)))) {
        GEXIT("Crypto error: encrypt", err);
    }

    gcry_cipher_close(aes);

    // Append HMAC
    gcry_md_hd_t hmac = compute_hmac(key, keysize, dp.data + 32, sizeof(buf));
    memcpy(dp.data, gcry_md_read(hmac, 0), 32);
    gcry_md_close(hmac);

    // Finish
    fill_out_checksum(&dp);

    // Send the message
    zmq_msg_t rmsg;
    errno = 0;
    zmq_msg_init_size(&rmsg, sizeof(dp));
    memcpy(zmq_msg_data(&rmsg), &dp, sizeof(dp));
    int s = zmq_sendmsg(context, &rmsg, 0);
    zmq_msg_close(&rmsg);
    return s;
}

bool decrypt_message(dpacket *dp, char *key, int keysize, char *out, int outsize) {
    gcry_error_t err;
    // Verify HMAC
    gcry_md_hd_t hmac = compute_hmac(key, keysize, dp->data + 32, 256);

    if (memcmp(dp->data, gcry_md_read(hmac, 0), 32)) {
        return false;
    }
    gcry_md_close(hmac);

    gcry_cipher_hd_t aes;
    if ((err = gcry_cipher_open(&aes, GCRY_CIPHER_AES256, GCRY_CIPHER_MODE_CTR, 0))) {
        GEXIT("Crypto error: open", err);
    }

    if ((err = gcry_cipher_setkey(aes, key, keysize))) {
        GEXIT("Crypto error: set key", err);
    }

    if ((err = gcry_cipher_setctr(aes, dp->id.id, sizeof(dp->id.id)))) {
        GEXIT("Crypto error: set ctr", err);
    }

    if ((err = gcry_cipher_decrypt(aes, out, outsize, dp->data + 32, 256))) {
        GEXIT("Crypto error: decrypt", err);
    }

    gcry_cipher_close(aes);

    return true;
}

int main(int argc, char **argv) {	
    WINDOW *message_win, *input_win;
    void *context = zmq_ctx_new();
    void *dealer = get_socket(context, ZMQ_DEALER);
	int startx, starty, width, height;
    int line = 1;
	int ch;
    char buf[80]; int idx = 0;
    char *nickname = "duser";
    bool connect = false, filter_on = false;
    u8 filter[16];
    int filter_len = -1;

    bzero(filter, sizeof(filter));

    while ((ch = getopt(argc, argv, "c:n:hvdf:")) != -1) {
        switch (ch) {
            case 'c':
                // host to connect to
                snprintf(buf, sizeof(buf) - 1, "tcp://%s", optarg);
                zmq_connect(dealer, buf);
                connect = true;
                break;
            case 'n':
                nickname = strdup(optarg);
                break;
            case 'd':
                debug++;
                break;
            case 'f':
                filter_len = strlen(optarg);
                if (filter_len > 0 && 
                        filter_len <= 32 && 
                        (filter_len & 1) == 0) {
                    int i = 0;
                    for (; i < filter_len / 2; i++)
                        filter[i] = byte_to_string(optarg, i);

                    filter_on = true;
                }
                else {
                    fprintf(stderr, "Format for -f incorrect\n");
                    print_help();
                    exit(EXIT_FAILURE);
                }
                break;
            case '?':
                if (optopt == 'n' || optopt == 'c')
                    fprintf (stderr, "Option -%c requires an argument\n", optopt);
                else if (isprint(optopt))
                    fprintf (stderr, "Unknown option -%c\n", optopt);
                else
                    fprintf (stderr, "Unknown option 0x%x\n", optopt);
            case 'h':
            case 'v':
            default:
                print_help();
                exit(EXIT_SUCCESS);
        }
    }

    if (!(connect && filter_on)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);

    if (connect_to_dinet(dealer, filter, filter_len) != sizeof(int)) {
        fprintf(stderr, "Couldn't send message\n");
        exit(EXIT_FAILURE);
    }

    zmq_pollitem_t items[] = {
        { dealer, 0, ZMQ_POLLIN, 0 }
    };

	initscr();			/* Start curses mode 		*/
    noecho();
	keypad(stdscr, TRUE);		/* I need that nifty F1 	*/

    // Prompt for password
    char password[256]; // TODO no swap
    int i = 0;
    printw("Enter password: ");
    while((ch = getch()) != ERR) {
        if (ch != KEY_ENTER && ch != '\n')
            password[i++] = ch;
        else
            break;
    }

    // Derive key
    char keybuf[32];
    gpg_err_code_t err;
    if ((err = gcry_kdf_derive(password, i, GCRY_KDF_PBKDF2, GCRY_MD_SHA256, salt, sizeof(salt),
                20000, sizeof(keybuf), keybuf))) {
        fprintf(stderr, "Key derivation error: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
        exit(EXIT_FAILURE);
    }

	halfdelay(2);
	height = LINES - 1;
	width = COLS;
	starty = 0;
	startx = 0;
	printw("Press F1 to exit");
	refresh();
	message_win = create_newwin(height, width, starty, startx, true);
	input_win = create_newwin(1, width, height, startx, false);

    wprintw(input_win, "> ");
    wrefresh(input_win);

    dpacket dp;
    zmq_msg_t rmsg;
	while((ch = wgetch(input_win)) != KEY_F(1)) {	
        if (ch == KEY_ENTER || ch == '\n') {
            buf[idx] = '\0';
            
            send_message(dealer, nickname, buf, keybuf, sizeof(keybuf), filter,
                    filter_len);

            wclear(input_win);
            wprintw(input_win, "> ");
            wrefresh(input_win);

            idx = 0;
        } 
        else if (ch == KEY_RESIZE ) {
            // TODO Handle resize
        }
        else if (ch != ERR) {
            buf[idx++] = (char)ch;
            waddch(input_win, ch);
            wrefresh(input_win);
        }
        else {
            // No keypress, check zeromq
            zmq_msg_init(&rmsg);
            zmq_poll(items, 1, 100);

            if (items[0].revents & ZMQ_POLLIN) {
                // Receive a message and display it
                zmq_recvmsg(dealer, &rmsg, 0);
                assert(zmq_msg_size(&rmsg) == sizeof(dp));
                memcpy(&dp, zmq_msg_data(&rmsg), sizeof(dp));

                const size_t cols = COLS - 2;
                char *line_buf = calloc(cols, 1);
                char sbuf[10];
                const time_t current_time = time(NULL);
                strftime(sbuf, sizeof(sbuf) - 1, "%H:%M:%S", localtime(&current_time));
                int used = snprintf(line_buf, cols, "%s <", sbuf);

                char outbuf[256];
                if (decrypt_message(&dp, keybuf, sizeof(keybuf), outbuf, sizeof(outbuf))) {
                    memcpy(line_buf + used, outbuf, cols - used - 1);

                    mvwaddstr(message_win, line++, 1, line_buf);
                    wrefresh(message_win);
                }
                
                free(line_buf);
                zmq_msg_close(&rmsg);
            }
        }
	}
		
	endwin();			/* End curses mode		  */
	return 0;
}

WINDOW *create_newwin(int height, int width, int starty, int startx, bool use_box)
{	
    WINDOW *local_win;

	local_win = newwin(height, width, starty, startx);
    if (use_box) {
        box(local_win, 0 , 0);	/* 0, 0 gives default characters 
                                    * for the vertical and horizontal
                                    * lines			*/
    }
	wrefresh(local_win);		/* Show that box 		*/

	return local_win;
}

