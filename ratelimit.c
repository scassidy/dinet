#include <netinet/in.h>
#include <sys/param.h>
#include <stdlib.h>
#include "ratelimit.h"
#include "common.h"
#include "uthash.h"

static inline void clean_map(struct minute_map **map) {
    struct minute_map *current, *tmp;

    HASH_ITER(hh, *map, current, tmp) {
        HASH_DEL(*map,current);
        free(current);
    }

    *map = NULL;
}

struct ratelimit *ratelimit_init(size_t threshold, int number, int divisor) {
    if (number < 2)
        return NULL;

    struct ratelimit *ptr = dmalloc(sizeof(struct ratelimit));

    ptr->threshold = threshold;
    ptr->number = number;
    ptr->maps = dcalloc(number, sizeof(struct minute_map**));
    ptr->last_accessed = time(NULL);
    ptr->divisor = divisor;

    return ptr;
}

bool ratelimit_check(struct ratelimit* handle, void *key, size_t key_len) {
    size_t total = 0;
    int i = 0;
    time_t now = time(NULL);

    time_t diff = (now - handle->last_accessed) / handle->divisor;
    time_t t = 0;

    // Cap at handle->number
    time_t moving = diff > handle->number ? handle->number : diff;

    for (; t < moving; t++)
        clean_map(&handle->maps[t]);

    if (moving > 0) {
        // [0000|AB] two left
        size_t remaining = handle->number - moving;
        // [AB|00AB] move them over
        memmove(handle->maps, handle->maps + moving, remaining * sizeof(struct minute_map*));
        // [AB|0000] zero the rest
        memset(handle->maps + remaining, 0, moving * sizeof(struct minute_map*));
        handle->last_accessed = now;
    }

    for (; i < handle->number; i++) {
        struct minute_map *tmp;
        int result = 0;

        HASH_FIND(hh, handle->maps[i], key, key_len, tmp);

        if (i == handle->number - 1) {
            if (tmp == NULL) {
                // If this is the latest map, add it
                tmp = dcalloc(1, sizeof(struct minute_map));
                size_t sz = MIN(sizeof(tmp->address), key_len);
                memcpy(&tmp->address, key, sz);
                tmp->count = 1;
                HASH_ADD(hh, handle->maps[i], address, sz, tmp);

                result = tmp->count;
            } else {
                tmp->count++;
                result = tmp->count;
            }
        }

        total += result * (i + 1) / handle->number;
    }

    return total > handle->threshold;
}

void ratelimit_deinit(struct ratelimit *handle) {
    int i = 0;
    for (; i < handle->number; i++)
        clean_map(&handle->maps[i]);
    free(handle->maps);
    handle->maps = NULL;
}


#ifdef MAIN
#include <unistd.h>
#undef NDEBUG
#include <assert.h>
int main(int argc, char **argv)
{
#define THRESH 100
    struct in_addr one, two;
    struct in6_addr ipv6;

    memset(&one, 1, sizeof(struct in_addr));
    memset(&two, 2, sizeof(struct in_addr));
    memset(&ipv6, 0xAB, sizeof(struct in6_addr));

    struct ratelimit *handle = ratelimit_init(THRESH, 1, 1);
    assert(handle == NULL);

    handle = ratelimit_init(THRESH, 2, 2);
    assert(handle != NULL);

    int i = 0;
    for (; i < THRESH; i++) {
        assert(!ratelimit_check(handle, &one, sizeof(one)));
        assert(!ratelimit_check(handle, &two, sizeof(two)));
    }

    for (i = 0; i < 5 * THRESH; i++) {
        if(ratelimit_check(handle, &one, sizeof(one)))
            break;
    }

    assert(i < 5 * THRESH);

    sleep(2);

    for (i = 0; i < 5 * THRESH; i++) {
        if(ratelimit_check(handle, &ipv6, sizeof(ipv6)))
            break;
    }

    assert(i < 5 * THRESH);

    ratelimit_deinit(handle);
    free(handle);

    return 0;
}
#endif

