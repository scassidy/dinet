#include <zmq.h>
#include <time.h>
#include "common.h"
#include "server.h"

void *get_socket(void *context, int type) {
    void *sock = zmq_socket(context, type);
    if (sock == NULL) {
        printf("get_socket: %d : %s\n", errno, zmq_strerror(errno));
        printf("     debug: EINVAL: %d, EFAULT: %d, EMFILE: %d, ETERM: %d\n", EINVAL, EFAULT, EMFILE, ETERM);
        exit(EXIT_FAILURE);
    }
    return sock;
}

bool receive_more(void *socket) {
    int64_t more;
    size_t more_sz;

    zmq_getsockopt(socket, ZMQ_RCVMORE, &more, &more_sz);
    return !!more;
}

void waitsleep(int milliseconds) {
    struct timespec t = {
        .tv_sec = milliseconds/1000,
        .tv_nsec = (milliseconds - (milliseconds/1000) * 1000) * 1000 * 1000
    };

    struct timespec rem;

    while (nanosleep(&t, &rem) == -1 && errno == EINTR) {
        t = rem;
    }
}

void print_packet(char *str, dpacket *dp, bool print_data) {
    int i;


    printf("%s id: 0x", str);

    for (i = 0; i < DINET_ID_SZ; i++) {
        printf("%02x", dp->id.id[i]);
    }

    printf(" checksum: 0x");
    for (i = 0; i < DINET_CHECKSUM_SZ; i++) {
        printf("%02x", dp->checksum[i]);
    }

    if (print_data) {
        printf(" data: 0x");
        for(i = 0; i < DINET_DATA_SZ; i++) { 
            printf("%02x", dp->data[i]);
        }
    }

    putchar('\n');
}

void gen_random(uint8_t *data, size_t len) {
    FILE *devrand = fopen("/dev/urandom", "r");

    if (devrand == NULL) 
        PE_QUIT("Couldn't open /dev/urandom");

    size_t cur = 0;

    while (cur < len) {
        cur += fread(data + cur, 1, len - cur, devrand);

        if (feof(devrand))
            PE_QUIT("Unexpected end of file on /dev/urandom");
        if (ferror(devrand))
            PE_QUIT("Error reading from /dev/urandom");
    }

    fclose(devrand);
}

void *dmalloc(size_t sz) {
    void *ptr = malloc(sz);
    if (ptr == NULL)
        PE_QUIT("malloc");
    
    return ptr;
}

void *dcalloc(size_t nmemb, size_t sz) {
    void *ptr = calloc(nmemb, sz);
    if (ptr == NULL)
        PE_QUIT("calloc");
    
    return ptr;
}

void *drealloc(void *ptr, size_t sz) {
    void *ret = realloc(ptr, sz);
    if (ret == NULL)
        PE_QUIT("calloc");
    
    return ret;
}
