#include <zmq.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <assert.h>
#include <signal.h>
#include <gcrypt.h>
#include "server.h"
#include "hash.h"
#include "uthash.h"
#include "common.h"
#include "edge.h"
#include "httpd.h"
#include "cache.h"

#define PTHREAD_FUNC void*(*)(void*)

int debug = 0;
static int cache_size = 1000;
static bool run = true;
static struct lru_cache cache;

int send_packet(void *socket, dpacket *odp) {
    zmq_msg_t message;
    int ret = 0;

    if (odp == NULL) {
        dpacket dp;

        gen_random(dp.id.id, sizeof(dp.id));
        gen_random(dp.data, sizeof(dp.data));
        fill_out_checksum(&dp);

        zmq_msg_init_size(&message, sizeof(dp));
        memcpy(zmq_msg_data (&message), &dp, sizeof(dp));
        
        ret = zmq_sendmsg(socket, &message, 0);
        zmq_msg_close(&message);

        print_packet("generated", &dp, false);

        add_to_cache(&cache, &dp, dp.checksum, DINET_CHECKSUM_SZ, NULL, false);
    } else {
        zmq_msg_init_size(&message, sizeof(dpacket));
        memcpy(zmq_msg_data (&message), odp, sizeof(dpacket));
        
        ret = zmq_sendmsg(socket, &message, 0);
        zmq_msg_close(&message);

        print_packet("forwarding", odp, false);
        add_to_cache(&cache, odp, odp->checksum, DINET_CHECKSUM_SZ, NULL, false);
    }
    return ret;
}

void print_help(void) {
    printf("dinetd v0.1\n");
    printf("Usage: dinetd -l *:4567 -s example.com:3432 -s dinet.org:3443\n");
    printf("\n-l h:p\t\tlisten on this address:port\n");
    printf("-L h:p\t\tlisten on this address:port for clients\n");
    printf("-H port\t\tlisten for HTTP GETs and POSTs on this port\n");
    printf("-s h:p\t\tconnect to this host:port\n");
    printf("-w ms\t\tGenerate fake data every so many milliseconds if there's no real data\n");
    printf("-d\t\tPrint debug output\n");
    printf("-h\t\tThis help\n");
}

static void s_signal_handler (int signal_value)
{
    run = false;
}

static void s_catch_signals (void)
{
    struct sigaction action;
    action.sa_handler = s_signal_handler;
    action.sa_flags = 0;
    sigemptyset (&action.sa_mask);
    sigaction (SIGINT, &action, NULL);
    sigaction (SIGTERM, &action, NULL);
}

int main (int argc, char **argv) {
    pthread_t edge, httpd_thread;
    int edge_listen_sz = 10, edge_listen_cnt = 0;
    char *edge_listen[edge_listen_sz + 1];
    void *context = zmq_ctx_new();
    void *publisher = get_socket(context, ZMQ_PUB);
    void *subscriber = get_socket(context, ZMQ_SUB);
    void *httpd = NULL;

    int sleeptime = 0;
    int c;
    char buf[80];
    bzero(buf, sizeof(buf));

    hash_init();
    init_cache(&cache, cache_size);
    s_catch_signals();

    while ((c = getopt(argc, argv, "c:dH:l:L:s:w:hv")) != -1) {
        switch (c) {
            case 'c':
                cache_size = strtol(optarg, NULL, 0);
                break;
            case 'd':
                debug++;
                break;
            case 'H':
                httpd = httpd_init(100, atoi(optarg));
                if (httpd == NULL) {
                    fprintf(stderr, "Couldn't start HTTP server: %s\n", strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            case 'l':
                snprintf(buf, sizeof(buf) - 1, "tcp://%s", optarg);
                dprintf("Listening on %s\n", buf);
                zmq_bind(publisher, buf);
                break;
            case 'L':
                if (edge_listen_cnt < edge_listen_sz) {
                    snprintf(buf, sizeof(buf) - 1, "tcp://%s", optarg);
                    dprintf("Listening for clients on %s\n", buf);
                    edge_listen[edge_listen_cnt] = strdup(buf);
                    edge_listen_cnt++;
                } else {
                    printf("Too many edge listens\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 's':
                snprintf(buf, sizeof(buf) - 1, "tcp://%s", optarg);
                dprintf("Connecting to %s\n", buf);
                zmq_connect(subscriber, buf);
                break;
            case 'w':
                sleeptime = strtol(optarg, NULL, 0);
                dprintf("Sleeping for %d ms on idle\n", sleeptime);
                break;
            case '?':
                if (optopt == 's' || optopt == 'l')
                    fprintf (stderr, "Option -%c requires an argument\n", optopt);
                else if (isprint(optopt))
                    fprintf (stderr, "Unknown option -%c\n", optopt);
                else
                    fprintf (stderr, "Unknown option 0x%x\n", optopt);
            case 'h':
            case 'v':
            default:
                print_help();
                exit(EXIT_SUCCESS);
        }
    }

    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, 0, 0);

    // Setup edge
    struct edge_data worker_data;
    if (edge_listen_cnt) {
        edge_listen[edge_listen_cnt] = NULL;
        zmq_bind(publisher, EDGE_SUBSCRIBE_INP);

        worker_data.listen_addrs = edge_listen;
        worker_data.run = true;
        worker_data.context = context;
        pthread_barrier_init(&worker_data.barrier, NULL, 2);

        pthread_create(&edge, NULL, (PTHREAD_FUNC)edge_worker, (void*) &worker_data);

        // this barrier is needed due to inproc sockets needing to
        // bind before connect
        pthread_barrier_wait(&worker_data.barrier);
        zmq_connect(subscriber, EDGE_PUBLISH_INP);
    }

    // Setup HTTPD
    struct httpd_data httpd_worker_data;
    if (httpd != NULL) {
        zmq_bind(publisher, HTTPD_SUBSCRIBE_INP); 

        httpd_worker_data.context = context;
        httpd_worker_data.run = true;
        pthread_barrier_init(&httpd_worker_data.barrier, NULL, 2);

        pthread_create(&httpd_thread, NULL, (PTHREAD_FUNC)httpd_worker, 
                (void*) &httpd_worker_data);

        // this barrier is needed due to inproc sockets needing to
        // bind before connect
        pthread_barrier_wait(&httpd_worker_data.barrier);
        zmq_connect(subscriber, HTTPD_PUBLISH_INP);
    }

    zmq_pollitem_t items[] = {
        { subscriber, 0, ZMQ_POLLIN, 0}
    };

    while(run) {
        dpacket dp;

        zmq_msg_t rmsg;
        zmq_msg_init(&rmsg);

        zmq_poll(items, 1, sleeptime);

        if (items[0].revents & ZMQ_POLLIN) {
            zmq_recvmsg(subscriber, &rmsg, 0);
            if (zmq_msg_size(&rmsg) == sizeof(dp)) {
                memcpy(&dp, zmq_msg_data(&rmsg), sizeof(dp));

                if (!in_cache(&cache, dp.checksum, DINET_CHECKSUM_SZ)) {
                    dprintf("not in cache!\n");
                    
                    if (valid_checksum(&dp)) {
                        add_to_cache(&cache, &dp, dp.checksum, DINET_CHECKSUM_SZ, NULL, false);
                        send_packet(publisher, &dp);
                    } else {   
                        print_packet("INVALID", &dp, true);
                        exit(EXIT_FAILURE);
                    }
                } 
                else {
                    dprintf("in cache\n");
                }
            }
            zmq_msg_close(&rmsg);
        }
        else
            send_packet(publisher, NULL);
    }

    dprintf("Interrupted...\n");

    if (edge_listen_cnt) {
        worker_data.run = false;
        pthread_join(edge, NULL);
    }

    if (httpd != NULL) {
        httpd_worker_data.run = false;
        pthread_join(httpd_thread, NULL);
    }

    httpd_destroy(httpd);
    zmq_close (publisher);
    zmq_close (subscriber);
    zmq_ctx_destroy (context);

    dprintf("Exiting\n");

    return 0;
}

