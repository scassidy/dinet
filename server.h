#ifndef __DINET_SERVER_H
#define __DINET_SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

extern int debug;

#define dprintf(format, ...) if (debug) fprintf (stderr, format, ##__VA_ARGS__)
#define d2printf(format, ...) if (debug >= 2) fprintf (stderr, format, ##__VA_ARGS__)
#define d3printf(format, ...) if (debug >= 3) fprintf (stderr, format, ##__VA_ARGS__)

#define PE_QUIT(msg) do { \
    perror(msg); \
    exit(EXIT_FAILURE); \
} while(0)

#define DINET_DATA_SZ 1024
#define DINET_CHECKSUM_SZ 32
#define DINET_ID_SZ 16

typedef union {
    u8 id[DINET_ID_SZ];
    struct {
        u64 high;
        u64 low;
    };
} id_number;

typedef struct {
    id_number id;
    u8 data[DINET_DATA_SZ];
    u8 checksum[DINET_CHECKSUM_SZ];
} __attribute__ ((packed)) dpacket;


#endif
